//dependencies

const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";
//used for authentication on own server
//once declared, cannot be changed

module.exports.createAccessToken = (user) => {
    console.log(user);

    //data object is created to contain some details of user
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };
    console.log(data);
    return jwt.sign(data, secret, {});
    //.sign creates a token and to prevent it from being edited
};

/* 
    1. You can only get access token when a user logs in with the correct credentials.

    2. As a user, you can only get your own details from your own token from logging in.
    
    3. JWT is not meant for sensitive data.
    
    4. JWT is like a passport, you can use around the app to access certain features meant for your type of user.


*/