//Dependencies
const User = require("../models/User");
const bcrypt = require("bcryptjs");

const auth = require("../auth");

//REGISTER USER
module.exports.registerUser = (req, res) => {
    console.log(req.body);
    //syntax of bcrypt:
    //bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)
    //saltrounds - no. of time to randomize the char
    //
    req.body;
    let passwordInput = req.body.password;

    const hashedPW = bcrypt.hashSync(passwordInput, 10);

    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
        mobileNumber: req.body.mobileNumber,
    });
    newUser
        .save()
        .then((user) => res.send(user))
        .catch((err) => res.send(err));
};

//retrieve All Users
module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//login user

module.exports.loginUser = (req, res) => {
    console.log(req.body);
    /* 
                  1. Find the user by the email
                  2. If found, we will check the password
                  3. If not found, we will send a message to the client. 
                  4. If user's input password is same as our stored password, we will generate token/key to access the app. If not, we will turn them away with a message. 
              */

    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("User not found in the database.");
            } else {
                //comparing of password
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );
                console.log(isPasswordCorrect);

                /* compareSync()
                                    will return a boolean value
                                    if it match=true, not match=false */
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send("Incorrect Password, please try again!");
                }
            }
        })
        .catch((err) => res.send(err));
};