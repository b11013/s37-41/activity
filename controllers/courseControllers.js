//Dependencies
const Course = require("../models/Course");
const bcrypt = require("bcryptjs");

const auth = require("../auth");

module.exports.registerCourse = (req, res) => {
    console.log(req.body);

    req.body;

    let newCourse = new Course({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    });
    newCourse
        .save()
        .then((course) => res.send(course))
        .catch((err) => res.send(err));
};

module.exports.getAllCourse = (req, res) => {
    Course.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};