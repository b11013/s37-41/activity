//DEPENDENCIES
const express = require("express");
const router = express.Router();

//imported module
const userControllers = require("../controllers/userControllers");

//ROUTES

//reg user
router.post("/", userControllers.registerUser);

//get all user
router.get("/", userControllers.getAllUsers);

//login user
router.post("/login", userControllers.loginUser);

module.exports = router;