const express = require("express");
const router = express.Router();

const courseControllers = require("../controllers/courseControllers");

router.post("/", courseControllers.registerCourse);

router.get("/", courseControllers.getAllCourse);

module.exports = router;